﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameoflife
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int a = 10;
            int b =20;
            int liczba_sasiadow = 0;
            int[,] tablica = new int[a,b];
            int[,] tablica_tmp = new int[a, b];
            
            for(int i=0;i<a;i++)
            {
                for(int j=0;j<b;j++)
                {
                    tablica[i, j] = rnd.Next(2);
                    Console.Out.Write(tablica[i,j] +" ");
                    tablica_tmp[i, j] = 0;
                }
                Console.Out.WriteLine();   
            }
            while (true)
            {
                for(int k=0; k<a; k++)
                {
                    for(int l=0; l<b; l++)
                    {
                        tablica_tmp[k, l] = 0;
                    }
                }
                for (int i = 0; i < a; i++)
                {
                    for (int j = 0; j < b; j++)
                    {
                        liczba_sasiadow = 0;
                        if (i == 0 && j == 0)
                        {
                            if (tablica[i + 1, j] == 1)
                                liczba_sasiadow++;
                            if (tablica[i, j + 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i + 1, j + 1] == 1)
                                liczba_sasiadow++;
                        }
                        else if (i == a - 1 && j == 0)
                        {
                            if (tablica[i - 1, j] == 1)
                                liczba_sasiadow++;
                            if (tablica[i - 1, j + 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i, j + 1] == 1)
                                liczba_sasiadow++;
                        }
                        else if (i == 0 && j == b - 1)
                        {
                            if (tablica[i, j - 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i + 1, j - 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i + 1, j] == 1)
                                liczba_sasiadow++;
                        }
                        else if (i == a - 1 && j == b - 1)
                        {
                            if (tablica[i - 1, j] == 1)
                                liczba_sasiadow++;
                            if (tablica[i - 1, j - 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i, j - 1] == 1)
                                liczba_sasiadow++;
                        }
                        else if (j == 0)
                        {
                            if(tablica[i-1, j] == 1)
                                liczba_sasiadow++;
                            if (tablica[i + 1, j] == 1)
                                liczba_sasiadow++;
                            if (tablica[i, j + 1] == 1)
                                liczba_sasiadow++;
                            if(tablica[i - 1, j+1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i + 1, j + 1] == 1)
                                liczba_sasiadow++;
                        }
                        else if (i == 0)
                        {
                            if (tablica[i, j-1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i, j+1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i+1, j + 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i +1, j - 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i + 1, j] == 1)
                                liczba_sasiadow++;
                        }
                        else if (i == a - 1)
                        {
                            if (tablica[i, j - 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i, j + 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i - 1, j + 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i - 1, j - 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i - 1, j] == 1)
                                liczba_sasiadow++;
                        }
                        else if (j == b - 1)
                        {
                            if (tablica[i, j - 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i+1, j - 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i + 1, j] == 1)
                                liczba_sasiadow++;
                            if (tablica[i - 1, j - 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i - 1, j] == 1)
                                liczba_sasiadow++;
                        }
                        else
                        {
                            if (tablica[i, j - 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i, j + 1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i + 1, j] == 1)
                                liczba_sasiadow++;
                            if (tablica[i - 1, j] == 1)
                                liczba_sasiadow++;
                            if (tablica[i - 1, j-1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i + 1, j+1] == 1)
                                liczba_sasiadow++;
                            if (tablica[i - 1, j +1 ] == 1)
                                liczba_sasiadow++;
                            if (tablica[i +1 , j-1] == 1)
                                liczba_sasiadow++;
                        }
                        if (tablica[i, j] == 1)
                        {
                            if (liczba_sasiadow < 2)
                                tablica_tmp[i, j] = 0;
                            if (liczba_sasiadow == 2 || liczba_sasiadow == 3)
                                tablica_tmp[i, j] = 1;
                            if (liczba_sasiadow > 3)
                                tablica_tmp[i, j] = 0;
                        }
                        else
                        {
                            if (liczba_sasiadow == 3)
                                tablica_tmp[i, j] = 1;
                        }

                    }
                }
                for (int i = 0; i < a; i++)
                {
                    for (int j = 0; j < b; j++)
                    {
                        tablica[i, j] = tablica_tmp[i, j];
                    }
                }
                Console.Clear();
                for (int i = 0; i < a; i++)
                {
                    for (int j = 0; j < b; j++)
                    {
                        Console.Out.Write(tablica[i, j] + " ");
                    }
                    Console.Out.WriteLine();
                }
           Console.ReadKey();
        }
        }
    }
}
